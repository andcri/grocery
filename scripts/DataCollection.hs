{-|
This file contains all the functions to get collect the data neded for the project
-}


{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module DataCollection where

import Lib
import Network.HTTP.Conduit
import Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.ByteString.Lazy as B
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.Arrays
import Database.PostgreSQL.Simple.Types
import Control.Applicative
import Data.Maybe
import GHC.Generics
import System.IO
import Data.Aeson.Types

-- json datatypes definitions
data FullResponse = FullResponse {recepies :: [Recepies]} deriving(Show, Generic)

data Recepies = Recepies { instructions :: String
                         , ingredients  :: [String]
                         , title        :: String } deriving(Show, Generic)

--database datatypes definitions
data DBRecepies = DBRecepies { _instructions :: String
                             , _ingredients  :: PGArray String
                             , _title        :: String} deriving(Generic, Show)


data DBRecepiesQuery = DBRecepiesQuery { get_id :: Int
                                       , get_instructions :: String
                                       , get_ingredients :: PGArray String
                                       , get_title :: String} deriving (Generic, Show)

data IngredientsDictionary = IngredientsDictionary { ingredient_names :: [String]} deriving (Generic, Show)
data UnitOfMeasureDictionary = UnitOfMeasureDictionary { uom :: [String] } deriving (Generic, Show)
data QuantitiesDictionary = QuantitiesDictionary { quant :: [String]} deriving (Generic, Show)

instance FromJSON QuantitiesDictionary
instance ToJSON QuantitiesDictionary
instance FromJSON UnitOfMeasureDictionary
instance ToJSON UnitOfMeasureDictionary
instance FromJSON IngredientsDictionary
instance ToJSON IngredientsDictionary
instance FromJSON Recepies
instance ToJSON Recepies
instance FromJSON FullResponse
instance ToJSON FullResponse

instance FromRow DBRecepiesQuery where
  fromRow = DBRecepiesQuery <$> field <*> field <*> field <*> field

instance FromRow DBRecepies where
  fromRow = DBRecepies <$> field <*> field <*> field

instance ToRow DBRecepies where
  toRow d = [ toField (_instructions d), toField (_ingredients d), toField (_title d)]

decodeJsonFile :: IO (Maybe FullResponse)
decodeJsonFile = fmap decode $ B.readFile "../datasets/recipes_raw_nosource_epi.json"

decodeIngredients :: IO (Maybe IngredientsDictionary)
decodeIngredients = fmap decode $ B.readFile "../datasets/ingredients_names.json"

decodeUnitOfMeasures :: IO (Maybe UnitOfMeasureDictionary)
decodeUnitOfMeasures = fmap decode $ B.readFile "../datasets/unit_of_measure.json"

decodeQuantities :: IO (Maybe QuantitiesDictionary)
decodeQuantities = fmap decode $ B.readFile "../datasets/quantities.json"

getQuantities = do
  result <- decodeQuantities
  case result of
    Nothing -> return []
    Just result -> return $ quant result

getUnitOfMeasures = do
  result <- decodeUnitOfMeasures
  case result of
    Nothing -> return []
    Just result -> return $ uom result

getIngredientsList = do
  result <- decodeIngredients
  case result of
    Nothing -> return []
    Just result -> return $ ingredient_names result

getFields :: [Recepies] -> [(String, [String], String)]
getFields [] = []
getFields (x : xs) = (instructions x, ingredients x, title x) : getFields xs

-- get recepies from database
getRecepies = do
  conn <- connect defaultConnectInfo { connectPassword = "MyPassword", connectDatabase = "foodpj" }
  recepies <- (query_ conn q :: IO [DBRecepiesQuery])
  return $ (fmap (get_id) recepies, fmap (fromPGArray . get_ingredients) recepies)
  --return $ fmap (fromPGArray . fromOnly) recepies
    where q = "select * from recepies order by id limit 100"

-- commit results to database and return 3 when the job is completed
commitToDatabase [] _ = return 3
commitToDatabase ((x, y, z) : xs) conn = do
  execute conn "insert into recepies \
               \(instructions, ingredients, title) \
               \values (?,?,?)" $ DBRecepies (x) (PGArray y) (z)
  commitToDatabase xs conn


-- TODO add function that present the title of the recepie taken from the database
--      the user will then insert a serving_type and a diet_type for the recepy
--      the function will then use a helper to update the row


main = do
  conn <- connect defaultConnectInfo { connectHost = "localhost", connectUser="postgres"
                      , connectPassword = "MyPassword", connectDatabase = "foodpj"}
  result <- decodeJsonFile
  case result of
    Nothing -> return 2
    Just result -> let recepie_list = getFields $ recepies result
                     in commitToDatabase recepie_list conn
