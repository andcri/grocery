{-|
This script will load all the ingredients in the database and extract for each the actual ingredients
needed and quantities (in this way (quantity, ingredient))

-}
--{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module IngredientsParser where

import DataCollection hiding (main, decodeJsonFile, getFields, commitToDatabase)

import Lib
import Network.HTTP.Conduit
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.ByteString.Lazy as B
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.Arrays
import Database.PostgreSQL.Simple.Types
import Data.Set as S
import GHC.Generics
import Data.Char
import Data.Aeson
import Data.Aeson.Types

--INFO: later this operations will be configured in a streaming fashion
-- query all the ingredient array from the db and parse them in a list
-- analyze the list and create a new one that extract the amount and ingredient name

data LexicalCategories = Ingredient { getIngredient :: String}
                      | Attribute { getAttribute :: String}
                      | Quantity { getQuantity :: String}
                      | UnitOfMeasure { getUnitOfMeasure :: String}
                      | End deriving (Show)

data IngredientsDictionary = IngredientsDictionary { ingredients :: [String]} deriving (Generic, Show)

lower :: String -> String
lower xs = fmap toLower xs

find_lexical_categories :: [String] -> Set String -> Set String -> Set String -> Set String -> [[LexicalCategories]]
find_lexical_categories (word : xs) ing attr quant uof = find_lexical_categories_in_string (fmap lower $ words word) ing attr quant uof : find_lexical_categories xs ing attr quant uof
find_lexical_categories [] _ _ _ _ = [[End]]

find_lexical_categories_in_string :: [String] -> Set String -> Set String -> Set String -> Set String -> [LexicalCategories]
find_lexical_categories_in_string (word : xs) ing attr quant uof = if S.member word ing
                                                                   then Ingredient word : find_lexical_categories_in_string xs ing attr quant uof
                                                                   else if S.member word attr
                                                                        then Attribute word : find_lexical_categories_in_string xs ing attr quant uof
                                                                        else if S.member word quant
                                                                             then Quantity word : find_lexical_categories_in_string xs ing attr quant uof
                                                                             else if S.member word uof
                                                                                  then UnitOfMeasure word : find_lexical_categories_in_string xs ing attr quant uof
                                                                                  else find_lexical_categories_in_string xs ing attr quant uof
find_lexical_categories_in_string [] _ _ _ _ = [End]


find_categories :: [[String]] -> Set String -> Set String -> Set String -> Set String -> [[[LexicalCategories]]]
find_categories (x : xs) ing attr quant uof = find_lexical_categories x ing attr quant uof : find_categories xs ing attr quant uof
find_categories [] _ _ _ _ = [[[End]]]

remove_punct :: String -> String
remove_punct xs = Prelude.filter (not . (`elem` ",.;")) xs

get_sentences :: [String] -> [[String]]
get_sentences (sentence : xs) = clean_sentence (words sentence)
                                  where clean_sentence y = [unwords $ Prelude.map remove_punct y] : get_sentences xs
get_sentences [] = []

case_check :: LexicalCategories -> String
case_check x = case x of
                 Ingredient _ -> "Ingredient"
                 Attribute _ -> "Attribute"

--removeEnds :: [[[LexicalCategories]]] -> [[[LexicalCategories]]]
removeEnds xs = do
  let step1 = init xs
  let step2 = fmap init step1
  let step3 = (fmap . fmap) init step2
  return step3

--removeEnds1 :: [[[LexicalCategories]]] -> [[[LexicalCategories]]]
--removeEnds1 xs = init xs >>= (\x1 -> fmap init x1)



parseRecepies = do
  --let fake_data = [ "1/2 cup celery, finely chopped", "1 small green pepper finely chopped", "1/2 cup finely sliced green onions", "1/4 cup chopped parsley", "1 pound crabmeat", "1 1/4 cups coarsely crushed cracker crumbs", "1/2 teaspoon salt", "3/4 teaspoons dry mustard", "Dash hot sauce", "1/4 cup heavy cream", "1/2 cup melted butter"]

  -- get recepie ingredient from database
  recepies_ingredients <- getRecepies

  -- get the dictionaries
  ingredients_list <- getIngredientsList
  unit_of_measure_list <- getUnitOfMeasures
  quantities <- getQuantities
  let ingredients = S.fromList $ (fmap . fmap) toLower ingredients_list
  -- all the different attribute of a more generic fruit/vegetable
  let attributes = S.fromList ["green", "red", "white", "yellow", "black", "blue", "violet", "orange"]
  let unit_of_measure = S.fromList $ (fmap . fmap) toLower unit_of_measure_list

  let quantity = S.fromList $ (fmap . fmap) toLower quantities

  -- retrieve the results
  let ingredients_found = find_categories (fmap concat $ fmap get_sentences (snd recepies_ingredients)) ingredients attributes quantity unit_of_measure

  final <- removeEnds ingredients_found
  return $ (fst recepies_ingredients, final)
