{-|
In here it lives the main program, it will query 3 * n_of_days recipes requested by the user and let the user choose
wich recipes he wants, after that we will print a grocieries list with all the ingredients needed
-}


{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module MainChooseRecepies where

import NumberAndUomParser hiding(first, second, third)
import Lib
import Network.HTTP.Conduit
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.ByteString.Lazy as B
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.Arrays
import Database.PostgreSQL.Simple.Types
import Data.Set as S
import GHC.Generics
import Data.Char
import Data.Aeson
import Data.Aeson.Types
import Control.Monad
import Data.List


data DBRecipes = DBRecipes { get_id           :: Int
                           , get_title        :: String} deriving(Generic, Show)

data DBRecipesInfo = DBRecipesInfo { g_ingredients :: PGArray String
                                   , g_attributes :: PGArray String
                                   , g_quantities :: PGArray String
                                   , g_unit_of_measure :: PGArray String
                                   , g_recipe_id :: Int} deriving(Generic, Show)

instance FromRow DBRecipes where
  fromRow = DBRecipes <$> field <*> field

instance FromRow DBRecipesInfo where
  fromRow = DBRecipesInfo <$> field <*> field <*> field <*> field <*> field


get_elements x = (fromPGArray . g_ingredients) x : (fromPGArray . g_attributes) x
                                   : (fromPGArray . g_quantities) x : (fromPGArray . g_unit_of_measure) x : []
                                   --[(show . g_recipe_id) x]

getRecipesInfo recipe_id = do
  conn <- connect defaultConnectInfo { connectPassword = "MyPassword", connectDatabase = "foodpj" }
  result <- (query conn "select ingredients, attributes, quantities, unit_of_measure, recepie_id \
                        \ from recepies_info where recepie_id = ?" (Only recipe_id) :: IO [DBRecipesInfo])
  return $ fmap get_elements result

--helper function that checks

--testing getting multiple recepies anc concat them together in a "grocery list" list
createList (x : recipes_selected) = liftM2 (++) (getRecipesInfo (x :: Int)) (createList recipes_selected)
createList [] = return []

--function to get itmes extracted from 4 length touple
get_ingredient (a , _ , _) = a
get_quantity   (_ , b , _) = b
get_unit       (_ , _ , c) = c

-- check in the list for equal ingredients and returns a list with them
values_to_sum (x : xs) = Prelude.filter (==(get_ingredient x)) (fmap get_ingredient xs) ++ values_to_sum xs
values_to_sum [] = []

-- filter for the values that are unique in the result list
values_without_sum (x : xs) (y : ys) = (Prelude.filter (\y -> if (join . get_ingredient) y == x then True else False) (y : ys)) : values_without_sum xs ys
values_without_sum [] _ = []

-- filter for the values that are NOT unique in the result list and put them in separate []
values_with_sum set (y : ys) = if S.member ((join . get_ingredient) y) set
                               then y : values_with_sum set ys
                               else values_with_sum set ys
values_with_sum _ [] = []

-- filter the values and put them in separate list for later ease of use
values_with_sum1 (x : xs) ys = helper x ys : values_with_sum1 xs ys
                                      where helper x (y : ys) = if x `elem` ((get_ingredient) y)
                                                                then y : helper x ys
                                                                else helper x ys
                                            helper _ [] = []
values_with_sum1 [] _ = []

-- helpers for parsing the duplicate values
all_equal []       = True
all_equal (x : xs) = all (== x) xs

first   (a , _ , _) = a
second  (_ , b , _) = b
third   (_ , _ , c) = c

safe_head (x : xs) = x
safe_head []       = ""

-- NOTE right now we do not sum the values like N but we concat them like strings
parseUnitsAndQuantities list = do
  filtered_units <- list
  duplicates     <- return $ third filtered_units
  units          <- return $ fmap concat $ second filtered_units
  quantities     <- return $ fmap ( (:[]) . sum . concat . (Prelude.filter (/=[]))) $ (fmap . fmap) get_quantity duplicates
  ingredients    <- return $ fmap ((:[]) . head . concat) $ (fmap . fmap) (get_ingredient) duplicates
  equals_list    <- return $ first filtered_units
  let test = build_list equals_list ingredients quantities units
  return test
    where build_list (e : es) (x : xs) (y : ys) (z : zs) =
            if e
            then [(x, y, [safe_head z])] : build_list es xs ys zs
            else let merged_z = [intercalate ", " z]
                     in [(x, y, merged_z)] : build_list es xs ys zs
          build_list [] _ _ _ = []
 -- return $ third filtered_units

-- Filter units of measure
  -- TODO remember that groupResult is a temporary way to get the data, filter units needs to be called from
  --      group_result and will pass the to_be_parsed_duplicate variable as arg
filterUnits list = do
  duplicates <- list
  units <- return $ fmap (Prelude.filter (/= [])) $ (fmap . fmap) (\x -> ((get_unit) x)) duplicates
  filtered_units <- return $ fmap (all_equal) units
  return (filtered_units, units, duplicates)


--parse the values in a printable manner and prints them on screen
parserForValues recipes_id = do
  values <- main recipes_id
  putStrLn "====================="
  putStrLn "Your Grocieries List:"
  putStrLn ""
  print_list values
  putStrLn "====================="
  return ()
    where print_list (x : xs) = do
            -- TODO check in unit that the array as lenght 1, if > then we need to append an OR between the values
            putStrLn $ (join . get_ingredient) x ++ " " ++ (show . round . head . get_quantity) x ++ " " ++ (join . get_unit) x
            print_list xs
          print_list _ = return ()

--retrieve result and collects the values by ingredient name
groupResult recipes_ids = do
  list                       <- createList recipes_ids
  return list

getRecipesTitle :: Int -> IO ()
getRecipesTitle n = do
  conn   <- connect defaultConnectInfo { connectPassword = "MyPassword", connectDatabase = "foodpj" }
  result <- (query conn "select id, title from recepies limit ?" (Only n) :: IO [DBRecipes])
  titles <- return $ fmap (get_title) result
  ids    <- return $ fmap (get_id) result
  let ids_titles = zipWith (,) ids titles
  print_title (fmap snd ids_titles) 0
  putStrLn "Choose the recipes separated by a space : "
  input  <- getLine
  let parsed_input = fmap read (Prelude.filter (/=" ") $ fmap (:[]) input) :: [Int]
  let list_ids = fmap (ids!!) parsed_input
  parserForValues list_ids
  --if (read input :: Int) <= n
    --then putStrLn $ show $ ids!!(read input :: Int)
    --else putStrLn "Please choose a recepie number" >> getRecipesTitle n

        where print_title (x : xs) n = do
                putStrLn $ (show n) ++ " " ++ x
                print_title xs (n+1)
              print_title [] _ = return ()


main recipes_ids = do
  result <- convertQuant $ groupResult recipes_ids
  multiple_occ               <- return $ Prelude.filter (/=[]) $ values_to_sum result
  result_single_occ          <- return $ Prelude.filter (\x -> if (Prelude.filter (==x) (join multiple_occ)) == [] then True else False) $ join $ fmap get_ingredient result
  result_multiple_occ        <- return $ Prelude.filter (/= "") $ concat $ values_to_sum result
  to_be_parsed_no_duplicates <- return $ values_without_sum result_single_occ result
  to_be_parsed_duplicates    <- return $ values_with_sum1 (S.toList (S.fromList result_multiple_occ)) result

  filtered_units             <- filterUnits $ return to_be_parsed_duplicates
  parsed_duplicates          <- parseUnitsAndQuantities $ return filtered_units
  return $ (concat parsed_duplicates) ++ (concat to_be_parsed_no_duplicates)
--  return parsed
