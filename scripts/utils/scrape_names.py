from bs4 import BeautifulSoup
import requests

letters = "abcdefghijklmnopqrstuvwxyz"

page = requests.get("https://www.bbcgoodfood.com/glossary/a")
soup = BeautifulSoup(page.content, 'html.parser')
data = []

print(page)

rows = soup.find_all('ul', {'class' : 'unstyled horizontal-elements row glossary-items'})
for row in rows:
    print(row)
