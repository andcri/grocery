from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup
import time

letters = "abcdefghijklmnopqrstuvwxyz"
ingredients = []


for letter in letters:

    url = f'https://www.bbcgoodfood.com/glossary/{letter}'
    req = Request(url , headers={'User-Agent': 'Mozilla/5.0'})
    
    webpage = urlopen(req).read()
    page_soup = soup(webpage, "html.parser")
    
    rows = page_soup.find('ul', {'class' : 'unstyled horizontal-elements row glossary-items'}).find_all('li')
    for row in rows:
        ingredients.append(row.find('a').find('h3').text)

    time.sleep(10)

print(ingredients)
