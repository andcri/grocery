import requests
from selenium import webdriver
from bs4 import BeautifulSoup as bs
from selenium.webdriver.chrome.options import Options
from sqlalchemy                     import create_engine, or_
from sqlalchemy.orm                 import sessionmaker
from sqlalchemy.ext.declarative     import declarative_base
from table_models import Recepies
import time

# STEPS
# read name of recepie from database
# scrape using the name of the recepy as query
# save the array with images to the database
#
uri = 'postgres+psycopg2://postgres:MyPassword@localhost:5432/foodpj'

engine   = create_engine(uri)
Session  = sessionmaker(bind=engine)
session  = Session()

rows = session.query(Recepies).order_by(Recepies.id).all()
print(type(rows))

counter = 0

for row in rows:

    if counter < 10:
        if row.pictures == None:
            print("new pic to add")

            recepie = row.title
            url = f"https://bing.com/images/search?q={recepie}"

            options = Options()
            options.headless = True
            driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver", chrome_options=options)
            driver.get(url)
            html = driver.page_source
            driver.close()

            soup = bs(html, 'html.parser')
            data = []

            elems = soup.find_all('div', {'class' : 'img_cont hoff'})

            for elem in elems:
                try:
                    src = elem.find('img')['src']
                    if src[:4] == 'data':
                        data.append(src)
                        counter = 0
                        break
                except:
                    pass


                # update db value
            if len(data) > 0:
                row.pictures = data
                session.commit()
                print("addedd")
            else:
                counter += 1
                print(counter)

            time.sleep(5)
        else:
            print("already has a pic, next")
    else:
        break
