from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, Float, create_engine
from sqlalchemy.dialects.postgresql import JSON, TIME, DATE, ARRAY, TIMESTAMP

Base = declarative_base()

class Recepies(Base):
    __tablename__= 'recepies'
    id           = Column(Integer, primary_key=True, autoincrement=True)
    instructions = Column(String, unique=True)
    ingredients  = Column(ARRAY(String))
    title        = Column(String)
    pictures     = Column(ARRAY(String))
