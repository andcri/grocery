{-|
collection of helpers that convert a string of the type
"4 + 4 + 1/2" into the algebic sum

collection of functions that convert a kitchen unit of measure
into their imperial sys equivalent or international equivalent

-}


{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--{-# LANGUAGE FlexibleContexts #-}

module NumberAndUomParser where

import Lib
import Network.HTTP.Conduit
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.ByteString.Lazy as B
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.Arrays
import Database.PostgreSQL.Simple.Types
import Data.Set as S
import GHC.Generics
import Data.Char
import Data.Aeson
import Data.Aeson.Types
import Control.Monad
import Data.List
import qualified Data.Map.Strict as Map

data NumberKind = NumberKind { whole_numbers      :: [String]
                             , fractional_numbers :: [String]} deriving (Generic, Show)

instance FromJSON NumberKind

decodeNumber :: IO (Maybe NumberKind)
decodeNumber = fmap decode $ B.readFile "../datasets/number_kind.json"

getNumbers = do
  result <- decodeNumber
  case result of
    Nothing -> return $ ([], [])
    Just r  -> return $ (whole_numbers r, fractional_numbers r)


first  (a,_,_) = a
second (_,b,_) = b
third  (_,_,c) = c

takeUntil :: String -> String -> String
takeUntil xs [] = []
takeUntil [] ys = []
takeUntil xs (y:ys) = if   isPrefixOf xs (y:ys)
                      then []
                      else y:(takeUntil xs (tail (y:ys)))

takeAfter :: Char -> String -> String
takeAfter x [] = ""
takeAfter x (y : ys) = if x == y
                       then ys
                       else takeAfter x ys


parse_fractional x = let numerator   = read $ takeUntil "/" x :: Int
                         denominator = read $ takeAfter '/' x :: Int
                         in (fromIntegral numerator :: Double) / (fromIntegral denominator :: Double)


--check_membership :: [String] -> [Double]
check_membership (x : xs) set = if S.member x set
                                then let converted_int = (read) x :: Int
                                         in (fromIntegral converted_int :: Double) : check_membership xs set
                                else parse_fractional x : check_membership xs set
check_membership [] _ = []

-- check the lenght of the string and if the number are fractional
parseNumber z numbers = let whole_numbers_set      = S.fromList $ fst numbers
                            fractional_numbers_set = S.fromList $ snd numbers
                          in case (length z) of
                            0 -> [0.0] :: [Double]
                            _ -> check_membership z whole_numbers_set
                       --then sum $ fmap (read) z :: Int
-- transform the quantities of the list
transform_list numbers (x : y : z : k) = ([intercalate " " (x ++ y)], ([sum (parseNumber z numbers)]) , concat k)

-- convert the quantity in the corrispondent unit of measure
convert_quant liquid weight quant uom = case lookup (concat uom) liquid of
                                          Nothing -> case lookup (concat uom) weight of
                                                     Nothing -> quant
                                                     Just r  -> fmap (*r) quant
                                          Just r  -> fmap (*r) quant

convert_uom liquid weight uom = case lookup (concat uom) liquid of
                                  Nothing -> case lookup (concat uom) weight of
                                               Nothing -> uom
                                               Just r  -> ["Kg"]
                                  Just r  -> ["Ml"]

-- transform the uom of the quantities to the list
-- multiply based on the value of the uom
transform_uom_list liquids weight x = let ing   = first x
                                          quant = second x
                                          uom   = third x
                                          in (ing, convert_quant liquids weight quant uom, convert_uom liquids weight uom)

-- get result from query
getResult :: IO [[[[Char]]]]
getResult = do
  let result = [[["ricotta"],[],["1"],["pound"]],[[],[],["2"],[]],[["butter"],[],["1"],["tablespoon"]],[["sage","nutmeg","lemon","zest"],[],["2","3"],[]],[[],[],["1/2","1/4"],["cup"]],[[],[],["1/4"],["teaspoon"]],[["flour","gnocchi"],[],[],[]],[["butter"],[],["8"],[]],[[],[],["2"],["teaspoons"]]]
  return result

-- here i will add a arg that represent the result
convertQuant res = do
  result                       <- res
  numbers                      <- getNumbers
  let unit_of_measure_metric_map_liquids = [("drop", 0.05), ("smidgen", 0.11), ("pinch", 0.23), ("dash", 0.46), ("saltspoon", 0.92)
                                                  , ("saltspoons", 0.92),("coffeespoon", 1.84), ("coffeespoons", 1.84), ("dram", 3.69)
                                                  , ("drams", 3.69), ("teaspoon", 4.92), ("teaspoons", 4.92), ("dessertspoon", 14.78)
                                                  , ("tablespoon", 14.78), ("tablespoons", 14.78), ("wineglass", 59.14)
                                                  , ("teacup", 118.2), ("cup", 236), ("cups", 236), ("pint", 473), ("quart", 946)
                                                  , ("pottle", 1892), ("gallon", 3785)]
  let unit_of_measure_metric_map_weight = [("pound", 0.45), ("pounds", 0.45), ("oz", 0.02), ("ozs", 0.02), ("ounce", 0.02), ("ounces", 0.02)]
  list_converted               <- return $ fmap (transform_list numbers) result
  final                        <- return $ fmap (transform_uom_list unit_of_measure_metric_map_liquids unit_of_measure_metric_map_weight) list_converted
  return final
