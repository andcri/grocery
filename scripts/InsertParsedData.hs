{-|
This file contanins all the functions to write the parsed data in the database in the correct format.
It also contains methods to check the missing values in the json file in order to be corrected manually
-}


{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module InsertParsedData where

import DataCollection hiding (main, decodeJsonFile, getFields, commitToDatabase)
import IngredientsParser

import Lib
import Network.HTTP.Conduit
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.ByteString.Lazy as B
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Database.PostgreSQL.Simple.Arrays
import Database.PostgreSQL.Simple.Types
import Data.Set as S
import GHC.Generics
import Data.Char
import Data.Aeson
import Data.Aeson.Types

data RecepiesInfo = RecepiesInfo { ingredients_info :: PGArray String
                                 , attributes_info  :: PGArray String
                                 , quantities_info  :: PGArray String
                                 , uom_info         :: PGArray String
                                 , recepie_id       :: Int} deriving (Generic, Show)

-- table that contains the ids of the recepies where an ingredient is not found in the json ingredients_names file
-- NOTE i query on 100 records, modifying manually ingredients_names
--      should be sufficient to ensure that the vocabulary will be big enough for the ingredients
--      later the same should be done for the rest (attr, quant, uom)
data RecepiesJsonMissing = RecepiesJsonMissing { recepies_id :: Int } deriving (Generic, Show)

instance ToRow RecepiesInfo where
  toRow d = [ toField (ingredients_info d), toField (attributes_info d), toField (quantities_info d), toField (uom_info d), toField (recepie_id d)]
instance ToRow RecepiesJsonMissing where
  toRow d = [ toField (recepies_id d) ]

get_ingredient_name :: LexicalCategories -> String
get_ingredient_name (Ingredient x) = x
get_ingredient_name _              = ""

get_attribute_name :: LexicalCategories -> String
get_attribute_name (Attribute x) = x
get_attribute_name _             = ""


get_quantity_name :: LexicalCategories -> String
get_quantity_name (Quantity x) = x
get_quantity_name _             = ""


get_unitofmeasure_name :: LexicalCategories -> String
get_unitofmeasure_name (UnitOfMeasure x) = x
get_unitofmeasure_name _             = ""

-- collect in a list the data for the different ingredients
--getIngredients :: [[[LexicalCategories]]] -> [[String]]
get_info_from_lexical (x : xs) f = one_level_deeper x f : get_info_from_lexical xs f
                                                     where one_level_deeper (x : xs) f = fmap f x : one_level_deeper xs f
                                                           one_level_deeper [] _ = []
get_info_from_lexical [] _ = []

concat_info xs = fmap concat xs


associate_info (x : xs) (y : ys) = get_info x y : associate_info xs ys
  where get_info (k : ks) (j : js) = ([k] ++ [j]) : get_info ks js
        get_info [] [] = []
associate_info [] [] = []


-- complete the function by sending data to db instead of printing and
-- change the passed id to the correct array
dbCommit _ [] [] [] [] _ = return ()
dbCommit (id : ids) (x : xs) (y : ys) (z : zs) (k : ks) conn = do
  one_level id x y z k conn
  dbCommit ids xs ys zs ks conn
    where one_level id (x : xs) (y : ys) (z : zs) (k : ks) conn = do
            --putStrLn $ show (id, x, y, z, k)
            execute conn "insert into recepies_info \
                         \(ingredients, attributes, quantities, unit_of_measure, recepie_id) \
                         \ values (?,?,?,?,?) " $ RecepiesInfo (PGArray x) (PGArray y) (PGArray z) (PGArray k) (id)
            one_level id xs ys zs ks conn
          one_level _ [] [] [] [] _ = return ()


find_missing _ [] [] [] [] _ = return ()
find_missing (id : ids) (x : xs) (y : ys) (z : zs) (k : ks) conn = do
  one_level id x y z k conn
  find_missing ids xs ys zs ks conn
    where one_level id (x : xs) (y : ys) (z : zs) (k : ks) conn = do
            if all (=="") x
            then do
                  execute conn "insert into recepies_json_missing_ingredients \
                              \ (recepies_id) values (?) " $ RecepiesJsonMissing (id)
                  return ()
            else one_level id xs ys zs ks conn

          one_level _ [] [] [] [] _ = return ()

--commitToDb [] [] [] [] _ = return 3
--commitToDb (x : xs) (y : ys) (z : zs) (k : ks) conn = do
--  execute conn "insert into recepies_info \
--               \(ingredients, attributes, quantities, unit_of_measure) \
--               \ values (?,?,?,?)" $ RecepiesInfo (PGArray x) (PGArray y) (PGArray z) (PGArray k)
--  commitToDb xs ys zs ks conn

main = do
  conn <- connect defaultConnectInfo { connectPassword = "MyPassword", connectDatabase = "foodpj" }
  parsed_recepies <- parseRecepies
  --let ingredients = fmap (Prelude.filter (/="")) $ concat_info $ get_info_from_lexical parsed_recepies get_ingredient_name
  --let attributes = fmap (Prelude.filter (/="")) $ concat_info $ get_info_from_lexical parsed_recepies get_attribute_name
  --let quantities = fmap (Prelude.filter (/="")) $ concat_info $ get_info_from_lexical parsed_recepies get_quantity_name
  --let unit_of_measures = fmap (Prelude.filter (/="")) $ concat_info $ get_info_from_lexical parsed_recepies get_unitofmeasure_name
  --commitToDb ingredients attributes quantities unit_of_measures conn
  let ingredients = (fmap . fmap) (Prelude.filter (/="")) $ get_info_from_lexical (snd parsed_recepies) get_ingredient_name
  let attributes = (fmap . fmap) (Prelude.filter (/="")) $ get_info_from_lexical (snd parsed_recepies) get_attribute_name
  let quantities = (fmap . fmap) (Prelude.filter (/="")) $ get_info_from_lexical (snd parsed_recepies) get_quantity_name
  let unit_of_measure = (fmap . fmap) (Prelude.filter (/="")) $ get_info_from_lexical (snd parsed_recepies) get_unitofmeasure_name
  let test = associate_info ingredients attributes
  find_missing (fst parsed_recepies) ingredients attributes quantities unit_of_measure conn
  -- INFO : uncomment this line to push all the parsed elements into the db
  --dbCommit (fst parsed_recepies) ingredients attributes quantities unit_of_measure conn
